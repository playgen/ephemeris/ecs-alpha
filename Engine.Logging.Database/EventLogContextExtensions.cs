﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Engine.Logging.Database.Model;

namespace Engine.Logging.Database
{
    public static class EventLogContextExtensions
    {
        public static IEnumerable<Event> GetEventsByGame(this EventLogContext context, Guid gameId)
        {
            return context.InstanceEvents.Where(ie => ie.GameId == gameId);
        }
    }
}
