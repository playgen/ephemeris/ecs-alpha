using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Engine.Logging.Database.Model
{
	public class PlayerIdentifier
	{
		public Guid GameId { get; set; }
		public int PlayerId { get; set; }
		public virtual Player Player { get; set; }


		public string IdentifierType { get; set; }

		public string Identifier { get; set; }
	}
}
